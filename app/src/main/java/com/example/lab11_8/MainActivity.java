package com.example.lab11_8;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onSecondTask(View v) {
        setContentView(R.layout.activity_second);
    }

    public void onThirdTask(View v) {
        setContentView(R.layout.myscreen);
    }
}